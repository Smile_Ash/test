<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* login */
Route::get('/', function () {
    return view('welcome');
});
Route::post('login', 'App\Http\Controllers\RegisterController@login');

/* registration */
Route::get('registerform', function () {
    return view('register');
});
Route::post('register', 'App\Http\Controllers\RegisterController@save');


Route::get('home', function(){
     return view('home');
});

Route::get('addressbookview', 'App\Http\Controllers\RegisterController@addressBook');
Route::get('addressbook', function () {
    return view('addressbook');
});
Route::get('add', 'App\Http\Controllers\RegisterController@add');
Route::get('edit/{id?}', 'App\Http\Controllers\RegisterController@edit');
Route::get('delete/{id?}', 'App\Http\Controllers\RegisterController@delete');
Route::post('addaddress/{id?}', 'App\Http\Controllers\RegisterController@addAddress');






