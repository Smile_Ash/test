@extends('layouts.app')
@section('main_content')   
       
        <button type="button" class="btn btn-primary"><a href="{{url('add')}}">Add</a></button>
        
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            
           @if ( Session::has('flash_message') )

           <div class="alert {{ Session::get('flash_type') }}">
               <h3>{{ Session::get('flash_message') }}</h3>
           </div>

           @endif
                        <table id="studentsTable" class="stripe" style="width:70%">
                            <thead>
                                <tr>
                                    <th scope="col">Actions</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($rec as $rows) { ?>
                                <tr>
                                <th scope="col">
                                    <div class="dropdown show">
                                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button"
                                            id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            Actions
                                        </a>

                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="{{url('edit/'.$rows->id)}}">Edit</a>
                                            <a class="dropdown-item" href="{{url('delete/'.$rows->id)}}">Delete</a>
                                        </div>
                                    </div>
                                </th>
                                <th scope="col">{{$rows->name}}</th>
                                <th scope="col">{{$rows->phone}}</th>
                                <th scope="col">{{$rows->email}}</th>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                
        </div>

           
        
@endsection       
   
