<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use DB;
use Illuminate\Support\Facades\Redirect;
use View;
use Exception;
use Session;
use Illuminate\Database\Schema\Blueprint;
use Schema;
use App\Models\UsersModel as Users;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function save(Request $request)
    { 
        try { 
            $name = $request->Input('name');
            $email = $request->Input('email');
            $phone = $request->Input('phone');
            $password = $request->Input('password');
            $userSchemaName = 'users'; //table name
            // $validated = Validator::make($request->all(), [
            //     'name' => 'required|min:3|alpha',
            //     'email' => 'required|email',
            // ]); 
            // if ($validator->fails()) {
            //     return redirect('registerform')
            //                 ->withErrors($validator)
            //                 ->withInput();
            // }
            if (Schema::hasTable($userSchemaName)) { // Check whether table exist
                // Inserts user values to the table 'users'
                $insert = array('name' => $name, 'email' => $email, 'phone' => $phone, 'password' => $password); //Values to be inserted
                $status = Users::insert($insert);
                if($status == 1){ // If the record has inserted successfully
                   return redirect('/'); // Redirects to login after registration  
                }
            } else { // if no table go back and inform user
                Session::flash('message', 'Some technical issues. Please contact the admin'); 
                return redirect('registerform'); // Redirecting back to registration
            }      
        } catch (\Exception $e) {
            if($e->getMessage()){ // If table have no user record redirect back with error message
                Session::flash('message', 'User already exist'); 
                return redirect('registerform'); // Redirecting back to registration
            }
        }      
    }

    public function login(Request $request)
    {
        try {
            $email = $request->Input('email');
            $password = $request->Input('password');
            $userSchemaName = 'users';
            if (Schema::hasTable($userSchemaName)) {     // Check whether table exist in the schema
                $rec = DB::table('users')->where('email', '=', $email)->where('password', '=', $password)->exists(); 
            }
            if($rec == 1){ //If table have the user record
               return redirect('home');  
            }else{
                Session::flash('message', 'Please double check your user name or password'); 
                return redirect('/');
            }
        } catch (\Exception $e) {

            if($e->getMessage()){ //If table have no user record redirect back with error message
                Session::flash('message', 'Please double check your user name or password'); 
                return redirect('/');
            }
        }    
        
    }
    public function addressBook(Request $request)
    {
        
        $rec = DB::table('users')->get(); 
        return View::make('addressbook')->with('rec', $rec);
        
    }

    public function edit($id)
    {
        $rec = DB::table('users')->where('id', $id)->first();
        //$arr = array('name' => $rec->name, 'name' => $rec->email, 'name' => $rec->phone);
        return View::make('add')->with('rec', $rec);

    }

    public function insert(Request $request)
    {
        $name = $request->Input('name');
        $email = $request->Input('email');
        $phone = $request->Input('phone');
        $password = $request->Input('password');
        $insert = array('name' => $name, 'email' => $email, 'phone' => $phone, 'password' => $password);
        DB::table('users')->insert($insert);
        return View::make('addressbookview');

    }

    public function delete($id)
    {
        
        $rec = DB::table('users')->where('id', $id)->delete();
        return redirect('addressbookview')->with('message', 'successfully deleted');

        
    }

    public function add(Request $request)
    {
        
        return View::make('add');
        
    }

    public function addAddress(Request $request, $id=0)
    {
        $name = $request->Input('name');
        $email = $request->Input('email');
        $phone = $request->Input('phone');
        $password = $request->Input('password');
        $insert = array('name' => $name, 'email' => $email, 'phone' => $phone, 'password' => $password);

        if ($id) {
            DB::table('users')->where('id', $id)->update($insert);
            return redirect('addressbookview')->with('message', 'successfully updated');
        }else{
            DB::table('users')->insert($insert);
            return View::make('addressbookview')->with('message', 'successfully added');
        }
        
    }
}
